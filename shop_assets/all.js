
$(document).ready(function(){
    mobMenu();
    $(".modal-wrapper").hide();
})

$(window).scroll(function(){
    header();
})

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function header(){
    if($(document).scrollTop() > 100){
        $(".logo").css({
            "width" : "50%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "8px",
            "transition" : "0.5s"
        })
    }
    else {
        $(".logo").css({
            "width" : "65%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "11px",
            "transition" : "0.5s"
        })
    }
}

function mobMenu(e) {
    if(e == 1){
        $(".mob-menu").css({
            'top'   :   '0'
        })
    }
    if(e == 2){
        $(".mob-menu").css({
            'top'   :   '-100vh'
        })
    }
}

$(function(){
    let h = $('.navbar-container').outerHeight();
    let s = $('.spacer-navi');
    $(s).css({
        'height' : ''+h

    })

})